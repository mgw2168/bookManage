from django.urls import path
from . import views

urlpatterns = [
    # 匹配书籍列表信息的URL,调用对应的bookList视图
    path(r'booklist/', views.bookList)
]