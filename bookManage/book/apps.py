from django.apps import AppConfig

# AppConfig.verbose_name属性用于设置该应用的直观可读的名字，此名字在Django提供的Admin管理站点中会显示
class BookConfig(AppConfig):
    name = 'book'
    verbose_name = '图书管理'